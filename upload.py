import datetime
import json
import sys
from pprint import pprint

import requests

from crypt import encrypt_file


root_url = 'http://107.170.152.228:31987'

def doit(infilepath, keyfilepath, encfilepath, username, password):
	encrypt_file(infilepath, keyfilepath, encfilepath, 'password')

	return upload_files(keyfilepath, encfilepath, username, password)



def upload_files(keyfilepath, encfilepath, username, password):
	token = get_token(username, password)

	files = {'key': open(keyfilepath, 'rb'),
	         'video': open(encfilepath, 'rb')}

	headers = {'Authorization': 'JWT {}'.format(token)}
	data = {'metadata': json.dumps({"length": 42,
	                                "uploaded_at": datetime.datetime.isoformat(datetime.datetime.now())})}

	video_upload_response = requests.post(
		'{}/end_user/videos/'.format(root_url),
		files=files, headers=headers, data=data)

	return video_upload_response, password


def get_token(username, password):
	print('logging in with {}, {}'.format(username, password))
	resp = requests.post(
		'{}/api-token-auth/'.format(root_url),
		data={'username': username, 'password': password}
	)
	if not resp.status_code == 200:
		print('Tried to login with {}, {}'.format(username, password))
		print(resp.text)
		resp.raise_for_status()
	return resp.json()['token']

def get_auth_headers(username, password):
	token = get_token(username, password)
	return {'Authorization': 'JWT {}'.format(token)}

def get_eu_videos(phone, auth_code):
	return requests.get('{}/end_user/videos/'.format(root_url), headers=get_auth_headers(phone, auth_code)).json()

def get_eu_videos_as_admin(username, password, video_id):
	return requests.get('{}/video/{}/'.format(root_url, video_id),
	                    headers=get_auth_headers(username, password)).json()

if __name__ == "__main__":
	video = sys.argv[1]
	username = sys.argv[2]
	password = sys.argv[3]
	resp, code = doit(video, './bbb.mp4.key', './bbb.mp4.enc', username, password)
	pprint(resp.json())