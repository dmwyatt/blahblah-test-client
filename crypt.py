import os
import struct
from typing import Tuple, BinaryIO, Iterator

from Crypto.Cipher import AES
from Crypto.Protocol.KDF import PBKDF2


ITERATIONS = 100000
SALT_SIZE_BYTES = 16
KEY_SIZE_BYTES = 16  # 128 bit key


def encrypt_file(videopath: str, keypath: str, destvideopath: str, password: str):
	key, salt = get_key(password, iterations=1)

	with open(keypath, 'wb') as f:
		f.write(key)

	video_size = os.path.getsize(videopath)
	with open(videopath, 'rb') as f:
		with open(destvideopath, 'wb') as encrypted_dest:
			encrypt(f, video_size, encrypted_dest, key, salt, os.urandom(AES.block_size))


def get_key(password: str, salt: bytes = None, iterations: int = ITERATIONS) -> Tuple[bytes, bytes]:
	"""
	Generates a 128-bit key and a salt.
	"""
	if salt is None:
		salt = os.urandom(SALT_SIZE_BYTES)

	return PBKDF2(password, salt, count=iterations), salt


def encrypt(infile: BinaryIO,
            infile_size: int,
            outfile: BinaryIO,
            key: bytes,
            salt: bytes,
            iv: bytes) -> None:
	"""
	Encrypts the contents of ``infile`` and stores them in ``outfile``.

	:param infile: Open read binary file handle of the file to encrypt
	:param infile_size: Size, in bytes, of the file to encrypt.
	:param outfile: Open write binary file handle of the file where we write encrypted contents.
	:param key: 128-bit bytes string key
	:param salt: salt used to create key.  Just pass 16 bytes (or SALT_SIZE_BYTES) of
				 whatever, if you don't care about storing the salt.  You might not
				 care if you're just using the key to decrypt instead of a password.
	:param iv: ``AES.block_size`` bytes to use as an initialization vector.  You could
			     generate that like this:  `os.urandom(AES.block_size)`
	"""
	assert len(salt) == SALT_SIZE_BYTES
	assert len(key) == KEY_SIZE_BYTES
	assert len(iv) == AES.block_size

	outfile.write(struct.pack('<Q', infile_size))
	outfile.write(salt)
	outfile.write(iv)

	encryptor = AES.new(key, AES.MODE_CBC, iv)

	while True:
		chunk = infile.read(1024 * AES.block_size)
		if len(chunk) == 0:
			break

		# last chunk not multiple of AES.block_size
		elif len(chunk) % AES.block_size != 0:
			padding_length = AES.block_size - (len(chunk) % AES.block_size)
			# we get rid of this padding by truncating to infile_size when decrypting
			chunk += b'\0' * padding_length

		outfile.write(encryptor.encrypt(chunk))


def decrypt_iter(infile: BinaryIO, key: bytes) -> Iterator[bytes]:
	"""
	Provides an iterator through decrypted contents of ``infile``.

	:param infile: Open read binary file handle of the file to encrypt
	:param key: 128-bit bytes string key
	"""
	size, salt, iv = read_file_header(infile)
	decryptor = AES.new(key, AES.MODE_CBC, iv)

	wrote = 0
	while True:
		chunk = infile.read(1024 * AES.block_size)
		if len(chunk) == 0:
			break

		decrypted = decryptor.decrypt(chunk)

		if wrote + len(decrypted) > size:
			# eliminate padding that we add to get a multiple of
			# AES.block_size when encoding
			last_chunk_size = size - wrote
			decrypted = decrypted[:last_chunk_size]

		yield decrypted

		wrote += len(decrypted)


def decrypt(infile: BinaryIO, outfile: BinaryIO, key: bytes) -> None:
	"""
	Decrypts the contents of ``infile`` and stores them in ``outfile``.
	:param infile: Open read binary file handle of the file to encrypt
	:param outfile: Open write binary file handle of the file where we
					write encrypted contents.
	:param key: 128-bit bytes string key
	"""
	for chunk in decrypt_iter(infile, key):
		outfile.write(chunk)


def read_file_header(infile: BinaryIO) -> Tuple[int, bytes, bytes]:
	"""
	Reads size, salt, and iv from file header.

	Leaves seek position at end of file header.
	"""
	size = infile.read(struct.calcsize('Q'))
	size = struct.unpack('<Q', size)[0]
	salt = infile.read(SALT_SIZE_BYTES)
	iv = infile.read(AES.block_size)
	return size, salt, iv
